import 'dart:ui';

import 'package:flutter/material.dart';
import 'package:xoo_arkray_new_wecheck_flt/input_event_menu_page/event_hypoglycemia.dart';
import '../input_event_menu_page/input_event_menu_list_page.dart';
import '../utilities/constants.dart';

abstract class MyMenuListItem {

  Widget buildLabel(BuildContext context);
  Widget buildSublabel(BuildContext context);
}

void routeInputEventMenuPage(BuildContext context){
  Navigator.of(context).push(
    MaterialPageRoute(builder: (context) => InputEventMenuPage())
  );
}

void routeEventHypoglycemiaPage(BuildContext context, String title){
  Navigator.of(context).push(
      MaterialPageRoute(builder: (context) => const EventHypoglycemiaPage())
  );
}

/// A ListItem that contains data to display a heading.
class HeaderItem implements MyMenuListItem {
  final String headerTitle;

  HeaderItem(this.headerTitle);

  @override
  Widget buildLabel(BuildContext context) {

    final kHeaderSmallDivider = Divider(
      height: 0.0,
      color: Theme.of(context).primaryColorDark,
      thickness: 2.0,
      indent: 20.0,
    );

    return Column(
        mainAxisAlignment: MainAxisAlignment.center,
        crossAxisAlignment: CrossAxisAlignment.start,
        children: <Widget>[
          Container(
            width: double.infinity,
            margin: EdgeInsets.zero,
            padding: EdgeInsets.zero,
            color: Theme.of(context).primaryColorDark,
            child: ListTile(
              minVerticalPadding: 0,
              contentPadding: EdgeInsets.symmetric(vertical: -5),
              dense: true,
              title: Text(
                "\t\t\t$headerTitle",
                style: TextStyle(
                  color: Colors.white,
                  fontWeight: FontWeight.bold,
                ),
              ),
            ),
          ),
          kHeaderSmallDivider,
        ]);
  }

  @override
  Widget buildSublabel(BuildContext context) => const SizedBox.shrink();
}

/// A ListItem that contains data to display a message.
class MenuItem implements MyMenuListItem {
  final String title;
  final String subtitle;
  final Future<String>? version;

  MenuItem(this.title, this.subtitle, {this.version});

  @override
  Widget buildLabel(BuildContext context) {

    const kSmallDivider = Divider(
      height: 0.0,
      color: kAccentColor,
      thickness: 2.0,
      indent: 20.0,
    );

    if (title == 'Version' && subtitle == '') {
      return Column(
        children: [
          ListTile(
              tileColor: Theme.of(context).canvasColor,
              minVerticalPadding: 15,
              contentPadding:
                  const EdgeInsets.symmetric(vertical: 10.0, horizontal: 16.0),
              dense: true,
              title: Text(title,
                  // tryString!,
                  style: const TextStyle(fontSize: 15.0)),
              trailing: FutureBuilder(
                future: version,
                builder:
                    (BuildContext context, AsyncSnapshot<String> snapshot) =>
                        Text(
                  snapshot.hasData ? snapshot.data! : "No Version Code Set",
                  style: const TextStyle(color: Colors.grey),
                ),
              ),
              onTap: () {
                onTapMenuRoutes(context, title);
              }),
          kSmallDivider,
        ],
      );
    } else if (subtitle == '') {
      return Column(
        children: [
          ListTile(
              tileColor: Theme.of(context).canvasColor,
              minVerticalPadding: 15,
              contentPadding:
                  const EdgeInsets.symmetric(vertical: 10.0, horizontal: 16.0),
              dense: true,
              title: Text(title,
                  // tryString!,
                  style: const TextStyle(fontSize: 15.0)),
              trailing: const Icon(Icons.arrow_forward_ios),
              onTap: () {
                onTapMenuRoutes(context, title);
              }),
          kSmallDivider,
        ],
      );
    } else {
      return Column(
        children: [
          ListTile(
            tileColor: Theme.of(context).canvasColor,
            isThreeLine: true,
            minVerticalPadding: 15,
            contentPadding:
                const EdgeInsets.symmetric(vertical: 10.0, horizontal: 16.0),
            dense: true,
            title: Text(
              title,
              style: const TextStyle(fontSize: 15.0),
            ),
            subtitle: Text(
              subtitle,
              maxLines: 1,
              overflow: TextOverflow.ellipsis,
              style: const TextStyle(fontSize: 12.0),
            ),
            trailing: const Icon(Icons.arrow_forward_ios),
            onTap: () {
              onTapMenuRoutes(context, title);
            },
          ),
          kSmallDivider,
        ],
      );
    }
  }

  @override
  Widget buildSublabel(BuildContext context) => const SizedBox.shrink();
}


void onTapMenuRoutes(
    BuildContext context,
    String title) {

  showMenuToast(context, title);

  // Provider.of<WeightListProvider>(context, listen: false).setHeaderDate(DateTime.now());

  if (title == bloodMenulistTitle[6]) {
    routeInputEventMenuPage(context);
  } else if (title == eventMenulistTitle[1]) {
    routeEventHypoglycemiaPage(context, title);
  } else if (title == eventMenulistTitle[2]) {
    routeEventHypoglycemiaPage(context, title);
  } else if (title == eventMenulistTitle[3]) {
    routeEventHypoglycemiaPage(context, title);
  } else if (title == eventMenulistTitle[4]) {
    routeEventHypoglycemiaPage(context, title);
  } else if (title == eventMenulistTitle[5]) {
    routeEventHypoglycemiaPage(context, title);
  } else if (title == eventMenulistTitle[6]) {
    routeEventHypoglycemiaPage(context, title);
  } else if (title == eventMenulistTitle[7]) {
    routeEventHypoglycemiaPage(context, title);
  } else if (title == eventMenulistTitle[8]) {
    routeEventHypoglycemiaPage(context, title);
  } 
}

void showMenuToast(BuildContext context, String title) {
  final scaffold = ScaffoldMessenger.of(context);
  scaffold.showSnackBar(
    SnackBar(
      content: Text(title),
      duration: const Duration(milliseconds: 400),
    ),
  );
}
