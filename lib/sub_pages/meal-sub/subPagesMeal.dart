import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:xoo_arkray_new_wecheck_flt/sub_pages/bg-sub/view_edit.dart';
import 'package:xoo_arkray_new_wecheck_flt/sub_pages/meal-sub/dailyGraph.dart';
import 'package:xoo_arkray_new_wecheck_flt/sub_pages/meal-sub/inputMeal.dart';
import 'package:xoo_arkray_new_wecheck_flt/sub_pages/meal-sub/viewEditMeal.dart';

class SubPagesMeal extends StatelessWidget {
  const SubPagesMeal({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      home: MyTabs(),
      debugShowCheckedModeBanner: false,
    );
  }
}

class MyTabs extends StatefulWidget {
  const MyTabs({Key? key}) : super(key: key);

  @override
  State<MyTabs> createState() => _MyTabsState();
}

class _MyTabsState extends State<MyTabs> with TickerProviderStateMixin {
  late TabController _tabController;

  @override
  void initState() {
    super.initState();
    _tabController = TabController(length: 3, vsync: this);
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
            title: const Text("Sample WeCheck"),
            elevation: 10,
            backgroundColor: Colors.blueGrey[900],
            bottom: TabBar(
                controller: _tabController,
                tabs: const <Widget>[
                  Tab(
                    text: 'Input Meal',
                  ),
                  Tab(
                    text: 'Daily Graph',
                  ),
                  Tab(
                    text: 'View/Edit Meal',
                  )
                ]
            )
        ),
        body: TabBarView(
            controller: _tabController,
            children: const [
              InputMeal(),
              DailyGraph(),
              ViewEditMeal()
            ]
        )
    );
  }
}

