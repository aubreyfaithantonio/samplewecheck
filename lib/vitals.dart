import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:xoo_arkray_new_wecheck_flt/sub_pages/bg-sub/subPagesBg.dart';
import 'package:xoo_arkray_new_wecheck_flt/sub_pages/vitals-sub/subPagesVitals.dart';
import 'package:xoo_arkray_new_wecheck_flt/utilities/constants.dart';

class Vitals extends StatefulWidget {
  const Vitals({Key? key}) : super(key: key);

  @override
  State<Vitals> createState() => _VitalsState();
}

class _VitalsState extends State<Vitals> {
  @override
  Widget build(BuildContext context) {
    return SubPagesVitals();
  }
}
