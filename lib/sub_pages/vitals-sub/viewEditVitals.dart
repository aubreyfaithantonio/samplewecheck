import 'package:flutter/material.dart';

class ViewEditVitals extends StatefulWidget {
  const ViewEditVitals({Key? key}) : super(key: key);

  @override
  State<ViewEditVitals> createState() => _ViewEditVitalsState();
}

class _ViewEditVitalsState extends State<ViewEditVitals> {
  @override
  Widget build(BuildContext context) {

    return ListView(
      children: [
        ListTile(
          contentPadding: EdgeInsets.all(5.0),
          leading: Icon(Icons.directions_walk, size: 35.0,),
          title: Text("View/Edit Steps"),
          subtitle: Text("You can view/edit steps"),
          trailing: Icon(Icons.arrow_forward_ios),
            onTap: (){
              ScaffoldMessenger.of(context).showSnackBar(
                SnackBar(
                  content: const Text('Steps'),
                  action: SnackBarAction(
                    label: 'Okay',
                    onPressed: () {
                      ScaffoldMessenger.of(context).hideCurrentSnackBar();
                    },
                  ),
                ),
              );
            }
        ),
        Divider(
          color: Colors.blueGrey[700],
          height: 1,
        ),
        ListTile(
          contentPadding: EdgeInsets.all(5.0),
          leading: Icon(Icons.monitor_weight, size: 35.0,),
          title: Text('View/Edit Weight'),
          subtitle: Text("You can view/edit weight"),
          trailing: Icon(Icons.arrow_forward_ios),
            onTap: (){
              ScaffoldMessenger.of(context).showSnackBar(
                SnackBar(
                  content: const Text('Weight'),
                  action: SnackBarAction(
                    label: 'Okay',
                    onPressed: () {
                      ScaffoldMessenger.of(context).hideCurrentSnackBar();
                    },
                  ),
                ),
              );
            }
        ),
        Divider(
          color: Colors.blueGrey[700],
          height: 1,
        ),
        ListTile(
          contentPadding: EdgeInsets.all(5.0),
          leading: Icon(Icons.favorite, size: 35.0,),
          title: Text('View/Edit Blood Pressure'),
          subtitle: Text("You can view/edit blood pressure"),
          trailing: Icon(Icons.arrow_forward_ios),
            onTap: (){
              ScaffoldMessenger.of(context).showSnackBar(
                SnackBar(
                  content: const Text('Blood Pressure'),
                  action: SnackBarAction(
                    label: 'Okay',
                    onPressed: () {
                      ScaffoldMessenger.of(context).hideCurrentSnackBar();
                    },
                  ),
                ),
              );
            }
        ),
        Divider(
          color: Colors.blueGrey[700],
          height: 1,
        ),
        ListTile(
          contentPadding: EdgeInsets.all(5.0),
          leading: Icon(Icons.thermostat, size: 35.0,),
          title: Text('View/Edit Body Temperature'),
          subtitle: Text("You can view/edit body temperature"),
          trailing: Icon(Icons.arrow_forward_ios),
            onTap: (){
              ScaffoldMessenger.of(context).showSnackBar(
                SnackBar(
                  content: const Text('Body Temperature'),
                  action: SnackBarAction(
                    label: 'Okay',
                    onPressed: () {
                      ScaffoldMessenger.of(context).hideCurrentSnackBar();
                    },
                  ),
                ),
              );
            }
        ),
        Divider(
          color: Colors.blueGrey[700],
          height: 1,
        ),
      ],
    );
  }
}
