import 'package:flutter/material.dart';

import '../item_data_source/menu_listitem.dart';
import '../utilities/constants.dart';


class MainBGPage extends StatelessWidget {
  MainBGPage({Key? key}) : super(key: key);

  final allDataMenuList = List<MyMenuListItem>.generate(
      bloodMenulistTitle.length,
          (i) => i % bloodMenulistTitle.length == 0
          ? HeaderItem(bloodMenulistTitle[i])
          : MenuItem(bloodMenulistTitle[i], bloodMenulistDesc[i]));

  //initialize event menu list
  final allEventMenuList = List<MyMenuListItem>.generate(
      eventMenulistTitle.length,
          (index) => index % eventMenulistTitle.length == 0
          ? HeaderItem(eventMenulistTitle[index])
          : MenuItem(eventMenulistTitle[index], eventMenulistDesc[index]));


  @override
  Widget build(BuildContext context) {

    return Scaffold(
        body: Column(
            mainAxisAlignment: MainAxisAlignment.start,
            children: <Widget>[

              Expanded(
                child: MediaQuery.removePadding(
                  context: context,
                  removeTop: true,
                  child: ListView.builder(
                      padding: EdgeInsets.zero,
                      itemCount: allDataMenuList.length,
                      itemBuilder: (context, index) {

                        final item = allDataMenuList[index];

                        return Stack(
                            children: <Widget> [
                              item.buildLabel(context),
                              item.buildSublabel(context),
                            ]
                        );
                      }
                  ),
                ),
              ),
            ]
        )
    );
  }
}
/*

class MainBGPage extends StatefulWidget {
  const MainBGPage({Key? key}) : super(key: key);

  @override
  _MainBGPageState createState() => _MainBGPageState();
}

class _MainBGPageState extends State<MainBGPage> {
  @override
  Widget build(BuildContext context) {

    final allDataMenuList = List<MyMenuListItem>.generate(
        bloodMenulistTitle.length,
            (i) => i % bloodMenulistTitle.length == 0
            ? HeaderItem(bloodMenulistTitle[i])
            : MenuItem(bloodMenulistTitle[i], bloodMenulistDesc[i]));

    //initialize event menu list
    final allEventMenuList = List<MyMenuListItem>.generate(
        eventMenulistTitle.length,
            (index) => index % eventMenulistTitle.length == 0
            ? HeaderItem(eventMenulistTitle[index])
            : MenuItem(eventMenulistTitle[index], eventMenulistDesc[index]));


    return Scaffold(
        body: Column(
            mainAxisAlignment: MainAxisAlignment.start,
            children: <Widget>[

              Expanded(
                child: MediaQuery.removePadding(
                  context: context,
                  removeTop: true,
                  child: ListView.builder(
                      padding: EdgeInsets.zero,
                      itemCount: allEventMenuList.length,
                      itemBuilder: (context, index) {

                        final item = allEventMenuList[index];

                        return Stack(
                            children: <Widget> [
                              item.buildLabel(context),
                              item.buildSublabel(context),
                            ]
                        );
                      }
                  ),
                ),
              ),
            ]
        )
    );
  }
}
*/

/// A ListItem that contains data to display a message.
class MenuItem implements MyMenuListItem {
  final String title;
  final String subtitle;
  final Future<String>? version;

  MenuItem(this.title, this.subtitle, {this.version});

  @override
  Widget buildLabel(BuildContext context) {

    final kSmallDivider = Divider(
      height: 0.0,
      color: kAccentColor,
      thickness: 2.0,
      indent: 20.0,
    );

    if (title == 'Version' && subtitle == '') {
      return Column(
        children: [
          Container(
            child: ListTile(
                tileColor: Theme.of(context).canvasColor,
                minVerticalPadding: 15,
                contentPadding:
                EdgeInsets.symmetric(vertical: 10.0, horizontal: 16.0),
                dense: true,
                title: Text(title,
                    // tryString!,
                    style: TextStyle(fontSize: 15.0)),
                trailing: FutureBuilder(
                  future: version,
                  builder:
                      (BuildContext context, AsyncSnapshot<String> snapshot) =>
                      Text(
                        snapshot.hasData ? snapshot.data! : "No Version Code Set",
                        style: TextStyle(color: Colors.grey),
                      ),
                ),
                onTap: () {
                  onTapMenuRoutes(context, title);
                }),
          ),
          kSmallDivider,
        ],
      );
    } else if (subtitle == '') {
      return Column(
        children: [
          Container(
            child: ListTile(
                tileColor: Theme.of(context).canvasColor,
                minVerticalPadding: 15,
                contentPadding:
                EdgeInsets.symmetric(vertical: 10.0, horizontal: 16.0),
                dense: true,
                title: Text(title,
                    // tryString!,
                    style: TextStyle(fontSize: 15.0)),
                trailing: Icon(Icons.arrow_forward_ios),
                onTap: () {
                  onTapMenuRoutes(context, title);
                }),
          ),
          kSmallDivider,
        ],
      );
    } else {
      return Column(
        children: [
          ListTile(
            tileColor: Theme.of(context).canvasColor,
            isThreeLine: true,
            minVerticalPadding: 15,
            contentPadding:
            EdgeInsets.symmetric(vertical: 10.0, horizontal: 16.0),
            dense: true,
            title: Text(
              title,
              style: TextStyle(fontSize: 15.0),
            ),
            subtitle: Text(
              subtitle,
              maxLines: 1,
              overflow: TextOverflow.ellipsis,
              style: TextStyle(fontSize: 12.0),
            ),
            trailing: Icon(Icons.arrow_forward_ios),
            onTap: () {
              onTapMenuRoutes(context, title);
            },
          ),
          kSmallDivider,
        ],
      );
    }
  }

  @override
  Widget buildSublabel(BuildContext context) => const SizedBox.shrink();
}