
import 'package:flutter/material.dart';

const kPrimaryColor = Color(0xFF000000);
const kPrimaryColorDark = Color(0xFF000000);
const kPrimaryColorDarkHeader = Color(0xff29B9D4);
const kAccentColor = Color(0xFFF1F1F1);
const kWhiteColor = Color(0xFFFFFFFF);
const kLightColor = Color(0xFF808080);
const kDarkColor = Color(0xFF303030);
const kTransparent = Colors.transparent;
const kTextBlueColor = Color(0xFF26c6da);
const kBgDarkBlueColor = Color(0xFF263238);

const List<Widget> toggleOnOff = <Widget>[
  Text('OFF'),
  Text('ON')
];


Widget mListView() {
  return ListView(
    padding: const EdgeInsets.all(15),
    children: <Widget>[
      Container(
        height: 75,
        color: Colors.white,
        child: const Center(child: Text('Import Blood Glucose')),
      ),
      Container(
        height: 75,
        color: Colors.white,
        child: const Center(child: Text('Input Blood Glucose Level')),
      ),
      Container(
        height: 75,
        color: Colors.white,
        child: const Center(child: Text('Input Insulin')),
      ),
      Container(
        height: 75,
        color: Colors.white,
        child: const Center(child: Text('Input Event')),
      )
    ],
  );
}

final bloodMenulistTitle = [
  'Top screen display (Line graph / Daily Graph)',
  'Input Blood Glucose Level',
  'Input Blood Glucose Level(Notebook Form)',
  'Input Insulin Pump',
  'Input Insulin',
  'Medication Log',
  'Input Event',
  'Input Memo/Photo',
  'Input A1C'
];

final bloodMenulistDesc = [
  '', //initialize 0
  'Let\'s input daily blood glucose levels', //Input Blood Glucose Level
  'Let\'s input daily blood glucose levels in notebook forms', //Input Blood Glucose Level(Notebook Form)
  'Input daily insulin pump', //Input Insulin Pump
  'Let\'s input daily insulin dose', //Input Insulin
  'Let\'s keep medication log', //Medication Log
  'Input daily activities', //Input Event
  'Let\'s input daily memo and picture', //Input Memo/Photo
  'Let\'s input monthly A1C', //Medical Records
];

final eventMenulistTitle = [
  'Input Event',
  'Hypoglycemia Awareness',
  'Sick Day',
  'Out of Bed',
  'Medicine(simple)',
  'Exercise',
  'Bathe',
  'Bedtime',
  'Customizable Event'
];

final eventMenulistDesc = [
  '',
  'Input when you feel Hypoglycemia awareness', //Hypoglycemia Awareness
  'Input when you are not well conditioned.', //Sick Day
  'Input when you wake up', //Out of Bed
  'Input when you take medicine', //Medicine(simple)
  'Input when you do exercise', //Exercise
  'Input when you take a bath', //Bathe
  'Input when you go to bed', //Bedtime
  'You can use as your event.', //Customizable Event
];
