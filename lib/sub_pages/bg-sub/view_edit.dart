import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class ViewEdit extends StatefulWidget {
  const ViewEdit({Key? key}) : super(key: key);

  @override
  State<ViewEdit> createState() => _ViewEditState();
}

class _ViewEditState extends State<ViewEdit> {
  @override
  Widget build(BuildContext context) {
    return Container(
        child: ListView(
          children: [
            ListTile(
              contentPadding: EdgeInsets.all(5.0),
              leading: Icon(Icons.picture_as_pdf, size: 35.0,),
              title: Text('Output to PDF'),
              subtitle: Text("Let's output data in PDF"),
              trailing: Icon(Icons.arrow_forward_ios),
                onTap: (){
                  ScaffoldMessenger.of(context).showSnackBar(
                    SnackBar(
                      content: const Text('Convert to PDF'),
                      action: SnackBarAction(
                        label: 'Okay',
                        onPressed: () {
                          ScaffoldMessenger.of(context).hideCurrentSnackBar();
                        },
                      ),
                    ),
                  );
                }
            ),
            Divider(
              color: Colors.blueGrey[700],
              height: 1,
            ),
            ListTile(
              contentPadding: EdgeInsets.all(5.0),
              leading: Icon(Icons.note, size: 35.0,),
              title: Text('Output to CSV'),
              subtitle: Text("Let's output data in CSV"),
              trailing: Icon(Icons.arrow_forward_ios),
                onTap: (){
                  ScaffoldMessenger.of(context).showSnackBar(
                    SnackBar(
                      content: const Text('Convert to CSV'),
                      action: SnackBarAction(
                        label: 'Okay',
                        onPressed: () {
                          ScaffoldMessenger.of(context).hideCurrentSnackBar();
                        },
                      ),
                    ),
                  );
                }
            ),
            Divider(
              color: Colors.blueGrey[700],
              height: 1,
            ),
            ListTile(
              contentPadding: EdgeInsets.all(5.0),
              leading: Icon(Icons.edit_location_alt, size: 35.0,),
              title: Text('View/Edit Blood Glucose Levels'),
              subtitle: Text("You can view/edit blood glucose levels"),
              trailing: Icon(Icons.arrow_forward_ios),
                onTap: (){
                  ScaffoldMessenger.of(context).showSnackBar(
                    SnackBar(
                      content: const Text('Blood Glucose Levels'),
                      action: SnackBarAction(
                        label: 'Okay',
                        onPressed: () {
                          ScaffoldMessenger.of(context).hideCurrentSnackBar();
                        },
                      ),
                    ),
                  );
                }
            ),
            Divider(
              color: Colors.blueGrey[700],
              height: 1,
            ),
            ListTile(
              contentPadding: EdgeInsets.all(5.0),
              leading: Icon(Icons.edit_note, size: 35.0,),
              title: Text('View/Edit Blood Glucose Levels (Notebook Form)'),
              subtitle: Text("You can view/edit blood glucose levels in a notebook form"),
              trailing: Icon(Icons.arrow_forward_ios),
                onTap: (){
                  ScaffoldMessenger.of(context).showSnackBar(
                    SnackBar(
                      content: const Text('Blood Glucose Levels (Notebook Form)'),
                      action: SnackBarAction(
                        label: 'Okay',
                        onPressed: () {
                          ScaffoldMessenger.of(context).hideCurrentSnackBar();
                        },
                      ),
                    ),
                  );
                }
            ),
            Divider(
              color: Colors.blueGrey[700],
              height: 1,
            ),
            ListTile(
              contentPadding: EdgeInsets.all(5.0),
              leading: Icon(Icons.edit, size: 35.0,),
              title: Text('View/Edit Insulin'),
              subtitle: Text("You can view/edit insulin"),
              trailing: Icon(Icons.arrow_forward_ios),
                onTap: (){
                  ScaffoldMessenger.of(context).showSnackBar(
                    SnackBar(
                      content: const Text('Insulin Dose'),
                      action: SnackBarAction(
                        label: 'Okay',
                        onPressed: () {
                          ScaffoldMessenger.of(context).hideCurrentSnackBar();
                        },
                      ),
                    ),
                  );
                }
            ),
            Divider(
              color: Colors.blueGrey[700],
              height: 1,
            ),
            ListTile(
              contentPadding: EdgeInsets.all(5.0),
              leading: Icon(Icons.edit_calendar, size: 35.0,),
              title: Text('View/Edit Event'),
              subtitle: Text("You can view/edit event"),
              trailing: Icon(Icons.arrow_forward_ios),
                onTap: (){
                  ScaffoldMessenger.of(context).showSnackBar(
                    SnackBar(
                      content: const Text('Events'),
                      action: SnackBarAction(
                        label: 'Okay',
                        onPressed: () {
                          ScaffoldMessenger.of(context).hideCurrentSnackBar();
                        },
                      ),
                    ),
                  );
                }
            ),
            Divider(
              color: Colors.blueGrey[700],
              height: 1,
            ),
            ListTile(
              contentPadding: EdgeInsets.all(5.0),
              leading: Icon(Icons.calendar_month, size: 35.0,),
              title: Text('Daily View'),
              subtitle: Text("See blood glucose levels, meals and insulin side-by-side"),
              trailing: Icon(Icons.arrow_forward_ios),
                onTap: (){
                  ScaffoldMessenger.of(context).showSnackBar(
                    SnackBar(
                      content: const Text('All glucose levels, meals and insulin'),
                      action: SnackBarAction(
                        label: 'Okay',
                        onPressed: () {
                          ScaffoldMessenger.of(context).hideCurrentSnackBar();
                        },
                      ),
                    ),
                  );
                }
            ),
            Divider(
              color: Colors.blueGrey[700],
              height: 1,
            )
          ],
        )
    );
  }
}
