import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:xoo_arkray_new_wecheck_flt/utilities/constants.dart';

class Others extends StatefulWidget {
  const Others({Key? key}) : super(key: key);

  @override
  State<Others> createState() => _OthersState();
}

class _OthersState extends State<Others> {

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text("Sample WeCheck"),
        elevation: 10,
        backgroundColor: Colors.blueGrey[900],
      ),
      body: ListView(
        children: [
          ListTile(
            contentPadding: EdgeInsets.all(10.0),
            leading: Icon(Icons.medical_information, size: 35.0,),
            title: Text('Clinical Inspection Data'),
            subtitle: Text("Let's register clinical inspection data (ex Result of a blood test at a medical institution"),
            trailing: Icon(Icons.arrow_forward_ios),
              onTap: (){
                ScaffoldMessenger.of(context).showSnackBar(
                  SnackBar(
                    content: const Text('You have pressed this item.'),
                    action: SnackBarAction(
                      label: 'Okay',
                      onPressed: () {
                        ScaffoldMessenger.of(context).hideCurrentSnackBar();
                      },
                    ),
                  ),
                );
              }
          ),
          Divider(
            color: Colors.blueGrey[700],
            height: 1,
          )
        ],
      )
    );
  }
}

// body: Card(
// margin: EdgeInsets.symmetric(vertical: 10.0, horizontal: 25.0),
// color: Colors.white,
// child: ListTile(
// leading: Icon(
// Icons.auto_graph,
// color: Colors.blueGrey,
// size: 35.0,
// ),
// title: Text(
// 'Clinical Inspection Data',
// style: TextStyle(
// color: Colors.blueGrey,
// fontWeight: FontWeight.bold,
// fontSize: 20.0
// ),
// ),
// subtitle: Text("Let's register clinical inspection data"),
// ),
// )
