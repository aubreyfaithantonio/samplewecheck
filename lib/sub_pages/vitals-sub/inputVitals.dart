import 'package:flutter/material.dart';

class InputVitals extends StatefulWidget {
  const InputVitals ({Key? key}) : super(key: key);

  @override
  State<InputVitals> createState() => _InputVitalsState();
}

class _InputVitalsState extends State<InputVitals> {
  @override
  Widget build(BuildContext context) {
    return ListView(
      children: [
        ListTile(
          contentPadding: EdgeInsets.all(5.0),
          leading: Icon(Icons.directions_walk, size: 35.0,),
          title: Text("Today's Steps"),
          subtitle: Text("Let's input daily blood glucose levels"),
          trailing: Icon(Icons.arrow_forward_ios),
            onTap: (){
              ScaffoldMessenger.of(context).showSnackBar(
                SnackBar(
                  content: const Text('Daily steps'),
                  action: SnackBarAction(
                    label: 'Okay',
                    onPressed: () {
                      ScaffoldMessenger.of(context).hideCurrentSnackBar();
                    },
                  ),
                ),
              );
            }
        ),
        Divider(
          color: Colors.blueGrey[700],
          height: 1,
        ),
        ListTile(
          contentPadding: EdgeInsets.all(5.0),
          leading: Icon(Icons.monitor_weight, size: 35.0,),
          title: Text('Input Weight'),
          subtitle: Text("Record daily weight"),
          trailing: Icon(Icons.arrow_forward_ios),
            onTap: (){
              ScaffoldMessenger.of(context).showSnackBar(
                SnackBar(
                  content: const Text('Weight'),
                  action: SnackBarAction(
                    label: 'Okay',
                    onPressed: () {
                      ScaffoldMessenger.of(context).hideCurrentSnackBar();
                    },
                  ),
                ),
              );
            }
        ),
        Divider(
          color: Colors.blueGrey[700],
          height: 1,
        ),
        ListTile(
          contentPadding: EdgeInsets.all(5.0),
          leading: Icon(Icons.favorite, size: 35.0,),
          title: Text('Input Blood Pressure'),
          subtitle: Text("Record daily blood pressure"),
          trailing: Icon(Icons.arrow_forward_ios),
            onTap: (){
              ScaffoldMessenger.of(context).showSnackBar(
                SnackBar(
                  content: const Text('Blood Pressure'),
                  action: SnackBarAction(
                    label: 'Okay',
                    onPressed: () {
                      ScaffoldMessenger.of(context).hideCurrentSnackBar();
                    },
                  ),
                ),
              );
            }
        ),
        Divider(
          color: Colors.blueGrey[700],
          height: 1,
        ),
        ListTile(
          contentPadding: EdgeInsets.all(5.0),
          leading: Icon(Icons.thermostat, size: 35.0,),
          title: Text('Input Body Temperature'),
          subtitle: Text("Record daily body temperature"),
          trailing: Icon(Icons.arrow_forward_ios),
            onTap: (){
              ScaffoldMessenger.of(context).showSnackBar(
                SnackBar(
                  content: const Text('Body Temperature'),
                  action: SnackBarAction(
                    label: 'Okay',
                    onPressed: () {
                      ScaffoldMessenger.of(context).hideCurrentSnackBar();
                    },
                  ),
                ),
              );
            }
        ),
        Divider(
          color: Colors.blueGrey[700],
          height: 1,
        )
      ],
    );
  }
}
