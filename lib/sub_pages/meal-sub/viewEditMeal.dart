import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class ViewEditMeal extends StatefulWidget {
  const ViewEditMeal({Key? key}) : super(key: key);

  @override
  State<ViewEditMeal> createState() => _ViewEditMealState();
}

class _ViewEditMealState extends State<ViewEditMeal> {
  @override
  Widget build(BuildContext context) {

    return Container(
      color: Colors.green[100],
      child: Center(
          child: Text('View/Edit Meal')
      ),
    );
  }
}
