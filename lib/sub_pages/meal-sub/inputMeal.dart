import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class InputMeal extends StatefulWidget {
  const InputMeal ({Key? key}) : super(key: key);

  @override
  State<InputMeal> createState() => _InputMealState();
}

class _InputMealState extends State<InputMeal> {
  @override
  Widget build(BuildContext context) {
    return Container(
      child: ListView(
        children: [
          ListTile(
            contentPadding: EdgeInsets.all(5.0),
            leading: Icon(Icons.dining, size: 35.0,),
            title: Text('Input Meal'),
            subtitle: Text("Let's input daily meal"),
            trailing: Icon(Icons.arrow_forward_ios),
              onTap: (){
                ScaffoldMessenger.of(context).showSnackBar(
                  SnackBar(
                    content: const Text('Input your meals'),
                    action: SnackBarAction(
                      label: 'Okay',
                      onPressed: () {
                        ScaffoldMessenger.of(context).hideCurrentSnackBar();
                      },
                    ),
                  ),
                );
              }
          ),
          Divider(
            color: Colors.blueGrey[700],
            height: 1,
          )
        ],
      ),
    );
  }
}
