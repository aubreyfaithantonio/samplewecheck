import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:xoo_arkray_new_wecheck_flt/sub_pages/bg-sub/subPagesBg.dart';
import 'package:xoo_arkray_new_wecheck_flt/sub_pages/meal-sub/subPagesMeal.dart';
import 'package:xoo_arkray_new_wecheck_flt/utilities/constants.dart';

class Meal extends StatefulWidget {
  const Meal({Key? key}) : super(key: key);

  @override
  State<Meal> createState() => _MealState();
}

class _MealState extends State<Meal> {
  @override
  Widget build(BuildContext context) {
    return SubPagesMeal();
  }
}
