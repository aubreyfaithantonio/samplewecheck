import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:xoo_arkray_new_wecheck_flt/utilities/constants.dart';

class Settings extends StatefulWidget {
  const Settings({Key? key}) : super(key: key);

  @override
  State<Settings> createState() => _SettingsState();
}

class _SettingsState extends State<Settings> {

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text("Sample WeCheck"),
        elevation: 10,
        backgroundColor: Colors.blueGrey[900],
      ),
      body: ListView(
        children: [
          InkWell(
              splashColor: Colors.blueGrey[50],
              hoverColor: Colors.blueGrey[100],
              onTap: (){
                ScaffoldMessenger.of(context).showSnackBar(
                  SnackBar(
                    content: const Text('You have pressed settings.'),
                    action: SnackBarAction(
                      label: 'Okay',
                      onPressed: () {
                        ScaffoldMessenger.of(context).hideCurrentSnackBar();
                      },
                    ),
                  ),
                );
              },
              child: ListTile(
                contentPadding: EdgeInsets.all(10.0),
                leading: Icon(Icons.settings, size: 35.0,),
                title: Text('Settings'),
                subtitle: Text("Various settings of WeCheck"),
                trailing: Icon(Icons.arrow_forward_ios),
              ),
          ),
          Divider(
            color: Colors.blueGrey[700],
            height: 1,
          ),
          InkWell(
              splashColor: Colors.blueGrey[50],
              hoverColor: Colors.blueGrey[100],
              onTap: (){
                ScaffoldMessenger.of(context).showSnackBar(
                  SnackBar(
                    content: const Text('You have pressed WeCheck Cloud.'),
                    action: SnackBarAction(
                      label: 'Okay',
                      onPressed: () {
                        ScaffoldMessenger.of(context).hideCurrentSnackBar();
                      },
                    ),
                  ),
                );
              },
              child: ListTile(
                contentPadding: EdgeInsets.all(10.0),
                leading: Icon(Icons.favorite, size: 35.0,),
                title: Text('Cooperation with WeCheck Cloud'),
                subtitle: Text("Data sync(Send and Receive) with WeCheck Cloud"),
                trailing: Icon(Icons.arrow_forward_ios),
              )
          ),
          Divider(
            color: Colors.blueGrey[700],
            height: 1,
          ),
          InkWell(
            splashColor: Colors.blueGrey[50],
            hoverColor: Colors.blueGrey[100],
            onTap: (){
              ScaffoldMessenger.of(context).showSnackBar(
                SnackBar(
                  content: const Text('You have pressed Bluetooth settings.'),
                  action: SnackBarAction(
                    label: 'Okay',
                    onPressed: () {
                      ScaffoldMessenger.of(context).hideCurrentSnackBar();
                    },
                  ),
                ),
              );
            },
            child: ListTile(
              contentPadding: EdgeInsets.all(10.0),
              leading: Icon(Icons.bluetooth_searching, size: 35.0,),
              title: Text('Bluetooth Settings'),
              subtitle: Text("You can set the data receiving in Bluetooth with blood glucose meter"),
              trailing: Icon(Icons.arrow_forward_ios),
            ),
          ),
          Divider(
            color: Colors.blueGrey[700],
            height: 1,
          ),
          InkWell(
            splashColor: Colors.blueGrey[50],
            hoverColor: Colors.blueGrey[100],
            onTap: (){
              ScaffoldMessenger.of(context).showSnackBar(
                SnackBar(
                  content: const Text('You have pressed NFC Data Reception.'),
                  action: SnackBarAction(
                    label: 'Okay',
                    onPressed: () {
                      ScaffoldMessenger.of(context).hideCurrentSnackBar();
                    },
                  ),
                ),
              );
            },
            child: ListTile(
              contentPadding: EdgeInsets.all(10.0),
              leading: Icon(Icons.sensors, size: 35.0,),
              title: Text('NFC Data Reception'),
              subtitle: Text("You can receive data from blood glucose meter, activity meter, thermometer with NFC communication function"),
              trailing: Icon(Icons.arrow_forward_ios),
            ),
          ),
          Divider(
            color: Colors.blueGrey[700],
            height: 1,
          ),
          InkWell(
              splashColor: Colors.blueGrey[50],
              hoverColor: Colors.blueGrey[100],
              onTap: (){
                ScaffoldMessenger.of(context).showSnackBar(
                  SnackBar(
                    content: const Text('You have pressed appointment reminder.'),
                    action: SnackBarAction(
                      label: 'Okay',
                      onPressed: () {
                        ScaffoldMessenger.of(context).hideCurrentSnackBar();
                      },
                    ),
                  ),
                );
              },
              child: ListTile(
                contentPadding: EdgeInsets.all(10.0),
                leading: Icon(Icons.add_alert, size: 35.0,),
                title: Text("Doctor's Appointment Reminder"),
                subtitle: Text("You can see Doctor's appointment data (data/time) on TOP screen"),
                trailing: Icon(Icons.arrow_forward_ios),
              )
          ),
          Divider(
            color: Colors.blueGrey[700],
            height: 1,
          ),
          InkWell(
            splashColor: Colors.blueGrey[50],
            hoverColor: Colors.blueGrey[100],
            onTap: (){
              ScaffoldMessenger.of(context).showSnackBar(
                SnackBar(
                  content: const Text('You have pressed About.'),
                  action: SnackBarAction(
                    label: 'Okay',
                    onPressed: () {
                      ScaffoldMessenger.of(context).hideCurrentSnackBar();
                    },
                  ),
                ),
              );
            },
            child: ListTile(
              contentPadding: EdgeInsets.all(10.0),
              leading: Icon(Icons.info, size: 35.0,),
              title: Text('About this App'),
              subtitle: Text("Find more information about this app"),
              trailing: Icon(Icons.arrow_forward_ios),
            ),
          ),
          Divider(
            color: Colors.blueGrey[700],
            height: 1,
          ),
          InkWell(
            splashColor: Colors.blueGrey[50],
            hoverColor: Colors.blueGrey[100],
            onTap: (){
              ScaffoldMessenger.of(context).showSnackBar(
                SnackBar(
                  content: const Text('You have pressed privacy policy.'),
                  action: SnackBarAction(
                    label: 'Okay',
                    onPressed: () {
                      ScaffoldMessenger.of(context).hideCurrentSnackBar();
                    },
                  ),
                ),
              );
            },
            child: ListTile(
              contentPadding: EdgeInsets.all(10.0),
              leading: Icon(Icons.lock, size: 35.0,),
              title: Text('Privacy Policy'),
              subtitle: Text("You can see the privacy policy"),
              trailing: Icon(Icons.arrow_forward_ios),
            ),
          ),
          Divider(
            color: Colors.blueGrey[700],
            height: 1,
          )
        ],
      )
    );
  }
}
