import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class Input extends StatefulWidget {
  const Input({Key? key}) : super(key: key);

  @override
  State<Input> createState() => _InputState();
}

class _InputState extends State<Input> {
  @override
  Widget build(BuildContext context) {
    return ListView(
          children: [
            Image.network('https://mba.hitbullseye.com/sites/default/files/mobilecommonImages/Line-Graph-Solved-Examples-01-03.jpg'),
            Divider(
              color: Colors.blueGrey[700],
              height: 1,
            ),
            ListTile(
              contentPadding: EdgeInsets.all(5.0),
              leading: Icon(Icons.water_drop, size: 35.0,),
              title: Text('Input Blood Glucose Level'),
              subtitle: Text("Let's input daily blood glucose levels"),
              trailing: Icon(Icons.arrow_forward_ios),
              onTap: () {
                ScaffoldMessenger.of(context).showSnackBar(
                  SnackBar(
                    content: const Text('Input your blood glucose levels.'),
                    action: SnackBarAction(
                      label: 'Okay',
                      onPressed: () {
                        ScaffoldMessenger.of(context).hideCurrentSnackBar();
                      },
                    ),
                  ),
                );
              },
            ),
            Divider(
              color: Colors.blueGrey[700],
              height: 1,
            ),
            ListTile(
              contentPadding: EdgeInsets.all(5.0),
              leading: Icon(Icons.book, size: 35.0,),
              title: Text('Input Blood Glucose Level (Notebook Form)'),
              subtitle: Text("Let's input daily blood glucose levels in Notebook Form"),
              trailing: Icon(Icons.arrow_forward_ios),
              onTap: () {
                ScaffoldMessenger.of(context).showSnackBar(
                  SnackBar(
                    content: const Text('Input your blood glucose levels in notebook form.'),
                    action: SnackBarAction(
                      label: 'Okay',
                      onPressed: () {
                        ScaffoldMessenger.of(context).hideCurrentSnackBar();
                      },
                    ),
                  ),
                );
              },
            ),
            Divider(
              color: Colors.blueGrey[700],
              height: 1,
            ),
            ListTile(
              contentPadding: EdgeInsets.all(5.0),
              leading: Icon(Icons.medication_liquid, size: 35.0,),
              title: Text('Input Insulin'),
              subtitle: Text("Let's input daily insulin dose"),
              trailing: Icon(Icons.arrow_forward_ios),
              onTap: (){
                ScaffoldMessenger.of(context).showSnackBar(
                  SnackBar(
                    content: const Text('Input your insulin dose.'),
                    action: SnackBarAction(
                      label: 'Okay',
                      onPressed: () {
                        ScaffoldMessenger.of(context).hideCurrentSnackBar();
                      },
                    ),
                  ),
                );
              },
            ),
            Divider(
              color: Colors.blueGrey[700],
              height: 1,
            ),
            ListTile(
              contentPadding: EdgeInsets.all(5.0),
              leading: Icon(Icons.directions_run, size: 35.0,),
              title: Text('Input Event'),
              subtitle: Text("Let's input daily activities"),
              trailing: Icon(Icons.arrow_forward_ios),
              onTap: (){
                ScaffoldMessenger.of(context).showSnackBar(
                  SnackBar(
                    content: const Text('Input your daily activities.'),
                    action: SnackBarAction(
                      label: 'Okay',
                      onPressed: () {
                        ScaffoldMessenger.of(context).hideCurrentSnackBar();
                      },
                    ),
                  ),
                );
              },
            ),
            Divider(
              color: Colors.blueGrey[700],
              height: 1,
            ),
          ],
        );
  }
}
