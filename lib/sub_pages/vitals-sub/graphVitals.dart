import 'package:flutter/material.dart';

class GraphVitals extends StatefulWidget {
  const GraphVitals({Key? key}) : super(key: key);

  @override
  State<GraphVitals> createState() => _GraphVitalsState();
}

class _GraphVitalsState extends State<GraphVitals> {
  @override
  Widget build(BuildContext context) {
    return ListView(
      children: [
        ListTile(
          contentPadding: EdgeInsets.all(5.0),
          leading: Icon(Icons.directions_walk, size: 35.0,),
          title: Text("Steps Graph"),
          subtitle: Text("Let's see the change of steps"),
          trailing: Icon(Icons.arrow_forward_ios),
            onTap: (){
              ScaffoldMessenger.of(context).showSnackBar(
                SnackBar(
                  content: const Text('Steps'),
                  action: SnackBarAction(
                    label: 'Okay',
                    onPressed: () {
                      ScaffoldMessenger.of(context).hideCurrentSnackBar();
                    },
                  ),
                ),
              );
            }
        ),
        Divider(
          color: Colors.blueGrey[700],
          height: 1,
        ),
        ListTile(
          contentPadding: EdgeInsets.all(5.0),
          leading: Icon(Icons.monitor_weight, size: 35.0,),
          title: Text('Weight Graph'),
          subtitle: Text("Let's see the change of weight"),
          trailing: Icon(Icons.arrow_forward_ios),
            onTap: (){
              ScaffoldMessenger.of(context).showSnackBar(
                SnackBar(
                  content: const Text('Weight'),
                  action: SnackBarAction(
                    label: 'Okay',
                    onPressed: () {
                      ScaffoldMessenger.of(context).hideCurrentSnackBar();
                    },
                  ),
                ),
              );
            }
        ),
        Divider(
          color: Colors.blueGrey[700],
          height: 1,
        ),
        ListTile(
          contentPadding: EdgeInsets.all(5.0),
          leading: Icon(Icons.favorite, size: 35.0,),
          title: Text('Blood Pressure Graph'),
          subtitle: Text("Let's see the change of blood pressure"),
          trailing: Icon(Icons.arrow_forward_ios),
            onTap: (){
              ScaffoldMessenger.of(context).showSnackBar(
                SnackBar(
                  content: const Text('Blood Pressure'),
                  action: SnackBarAction(
                    label: 'Okay',
                    onPressed: () {
                      ScaffoldMessenger.of(context).hideCurrentSnackBar();
                    },
                  ),
                ),
              );
            }
        ),
        Divider(
          color: Colors.blueGrey[700],
          height: 1,
        ),
        ListTile(
          contentPadding: EdgeInsets.all(5.0),
          leading: Icon(Icons.thermostat, size: 35.0,),
          title: Text('Body Temperature Graph'),
          subtitle: Text("Let's see the change of body temperature"),
          trailing: Icon(Icons.arrow_forward_ios),
            onTap: (){
              ScaffoldMessenger.of(context).showSnackBar(
                SnackBar(
                  content: const Text('Body Temperature'),
                  action: SnackBarAction(
                    label: 'Okay',
                    onPressed: () {
                      ScaffoldMessenger.of(context).hideCurrentSnackBar();
                    },
                  ),
                ),
              );
            }
        ),
        Divider(
          color: Colors.blueGrey[700],
          height: 1,
        )
      ],
    );
  }
}
