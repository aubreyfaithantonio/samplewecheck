

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:xoo_arkray_new_wecheck_flt/item_data_source/menu_listitem.dart';

import '../utilities/constants.dart';

Widget callTextDisplay(){

  return const Padding(
    padding: EdgeInsets.all(8.0),
    child: Text(
      "Memo",
      textAlign: TextAlign.left,
      style: TextStyle(
        color: Colors.white,
        fontSize: 15.0,),
    ),
  );
}

class EventHypoglycemiaPage extends StatefulWidget {
  const EventHypoglycemiaPage({Key? key}) : super(key: key);

  @override
  State<EventHypoglycemiaPage> createState() => _EventHypoglycemiaPageState();
}

class _EventHypoglycemiaPageState extends State<EventHypoglycemiaPage> {

  final List<bool> _selectedToggleOnOFf = <bool>[false, true];

  @override
  Widget build(BuildContext context) {

    final ButtonStyle style =
    ElevatedButton.styleFrom(textStyle: const TextStyle(fontSize: 20));


    return Scaffold(
      appBar: AppBar(
        title: Text(eventMenulistTitle[1])
      ),

      body: Column(
        mainAxisAlignment: MainAxisAlignment.start,
        children: <Widget>[

          /*Row1*/
          Container(
            height: 130,
            color: Colors.grey.shade700,
            child: Row (
              children: <Widget>[

                Expanded(
                  flex: 1,
                  child: Padding(
                    padding: EdgeInsets.all(8.0),
                    child: Column(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: <Widget>[

                        const Text(
                          "Nov. 2022",
                          textAlign: TextAlign.left,
                          style: TextStyle(
                            color: Colors.white,
                            fontSize: 13.0,),
                        ),
                        Row(
                          children: const <Widget> [

                            Text(
                              "22",
                              textAlign: TextAlign.left,
                              style: TextStyle(
                                color: Colors.white,
                                fontSize: 25.0,),
                            ),

                            Text(
                              "(Tue)",
                              textAlign: TextAlign.left,
                              style: TextStyle(
                                color: Colors.white,
                                fontSize: 13.0,),
                            ),
                          ],
                        ),

                      ],
                    ),
                  ),
                ),
                const Expanded(
                  flex: 3,
                  child: Padding(
                    padding: EdgeInsets.all(15.0),
                    child: Text(
                      "14:09",
                      textAlign: TextAlign.right,
                      style: TextStyle(
                        color: Colors.white,
                        fontSize: 32.0,),
                    ),
                  ),
                ),
                Expanded(
                  flex: 2,
                  child: Padding(
                    padding: EdgeInsets.all(8.0),
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                      crossAxisAlignment: CrossAxisAlignment.center,
                      children: const [

                        ElevatedButton(
                          style: ButtonStyle(

                          ),
                          onPressed: null,
                          child: Text(
                            "Date",
                            style: TextStyle(
                                color: Colors.white,
                                fontSize: 12),
                          ),
                        ),
                        ElevatedButton(
                          style: null,
                          onPressed: null,
                          child: Text(
                            "Now",
                            style: TextStyle(
                                color: Colors.white,
                                fontSize: 12),
                          ),
                        ),

                      ],
                    ),
                  ),
                ),
              ],
            ),
          ),

          /*Row2*/
          Container(
            width: double.infinity,
            height: 40,
            color: Colors.grey,
            child: const Expanded(
                child: Padding(
                  padding: EdgeInsets.all(8.0),
                  child: Text(
                    "Memo",
                    textAlign: TextAlign.left,
                    style: TextStyle(
                        fontSize: 15.0,),
                  ),
                ),
            ),
          ),

          /*Row3*/
          Expanded(
            flex: 4,
              child: Container(
                width: double.infinity,
                height: double.infinity,
                color: Colors.grey.shade400,
                child: Padding(
                  padding: const EdgeInsets.all(8.0),
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.start,
                    children: [

                      const SizedBox(
                        width: double.infinity,
                        child: ElevatedButton(
                          style: ButtonStyle(
                            backgroundColor: MaterialStatePropertyAll(Colors.white),
                          ),
                          onPressed: null,
                          child: Text(
                            "Copy the previous data",
                            style: TextStyle(
                                color: Colors.grey,
                                fontSize: 15),
                          ),
                        ),
                      ),
                      const Padding(
                        padding: EdgeInsets.only(top: 8.0, bottom: 8),
                        child: ColoredBox(
                          color: Colors.white70,
                          child: TextField(
                            cursorColor: Colors.grey,
                            obscureText: false,
                            maxLines: 4,
                            decoration: InputDecoration(
                              border: OutlineInputBorder(
                              ),
                              labelText: 'Input Memo',
                            ),
                          ),
                        ),
                      ),
                      Padding(
                        padding: const EdgeInsets.all(8.0),
                        child: Row(
                          children: <Widget>[
                            const Expanded(
                              child: Text(
                                  'Display this event in PDF',
                                  textAlign: TextAlign.left,
                              ),
                            ),
                            ToggleButtons(
                              borderColor: Colors.black45,
                              selectedColor: Colors.black12,
                              isSelected: _selectedToggleOnOFf,
                              children: toggleOnOff,
                            ),
                          ],
                        ),
                      ),
                      Expanded(
                        child: Container(
                          alignment: Alignment.bottomCenter,
                          child: Padding(
                            padding: const EdgeInsets.all(8.0),
                            child: Row(
                              mainAxisAlignment: MainAxisAlignment.spaceBetween,
                              children: <Widget>[
                                Expanded(
                                  child: SizedBox.fromSize(
                                    size: const Size(55, 55),
                                    child: ClipOval(
                                      child: Material(
                                        color: Colors.pink,
                                        child: InkWell(
                                          splashColor: Colors.white70,
                                          onTap: () {},
                                          child: Column(
                                            mainAxisAlignment: MainAxisAlignment.center,
                                            children: const <Widget>[
                                              Icon(Icons.bloodtype_rounded,
                                              color: Colors.white,),
                                            ],
                                          ),
                                        ),
                                      ),
                                    ),
                                  )
                                ),
                                const Expanded(
                                  flex: 7,
                                  child: ElevatedButton(
                                    style: ButtonStyle(
                                      backgroundColor: MaterialStatePropertyAll(Colors.pink),
                                    ),
                                    onPressed: null,
                                    child: Text(
                                      "Input",
                                      style: TextStyle(
                                          color: Colors.white,
                                          fontSize: 20),
                                    ),
                                  ),
                                ),
                              ],
                            ),
                          ),
                        ),
                      )
                    ],
                  ),
                ),
              )
          ),
        ],

      ),
    );
  }
}

