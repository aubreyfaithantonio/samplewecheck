import 'package:flutter/material.dart';

class DailyGraph extends StatefulWidget {
  const DailyGraph({Key? key}) : super(key: key);

  @override
  State<DailyGraph> createState() => _DailyGraphState();
}

class _DailyGraphState extends State<DailyGraph> {
  @override
  Widget build(BuildContext context) {
    return Container(
        color: Colors.blue[100],
        child: Center(
            child: Text('Daily Graph')
        )
    );
  }
}
