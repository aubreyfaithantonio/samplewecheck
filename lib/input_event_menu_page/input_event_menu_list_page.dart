

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:xoo_arkray_new_wecheck_flt/item_data_source/menu_listitem.dart';

import '../utilities/constants.dart';

class InputEventMenuPage extends StatelessWidget {
  InputEventMenuPage({Key? key}) : super(key: key);

  //initialize event menu list
  final allEventMenuList = List<MyMenuListItem>.generate(
      eventMenulistTitle.length,
          (index) => index % eventMenulistTitle.length == 0
  ? HeaderItem(eventMenulistTitle[index])
  : MenuItem(eventMenulistTitle[index], eventMenulistDesc[index]));

  @override
  Widget build(BuildContext context) {

    return Scaffold(
      appBar: AppBar(
        title: const Text("Sample App bar Title")
      ),
      body: Column(
        mainAxisAlignment: MainAxisAlignment.start,
        children: <Widget>[

          Expanded(
              child: MediaQuery.removePadding(
                  context: context,
                  removeTop: true,
                  child: ListView.builder(
                    padding: EdgeInsets.zero,
                    itemCount: allEventMenuList.length,
                    itemBuilder: (context, index) {

                      final item = allEventMenuList[index];

                      return Stack(
                        children: [
                          item.buildLabel(context),
                          item.buildSublabel(context)
                        ],
                      );
                    },
                  ))),
        ],

      ),
    );
  }
}


/// A ListItem that contains data to display a message.
class MenuItem implements MyMenuListItem {

  final String title;
  final String subtitle;
  final Future<String>? version;

  MenuItem(this.title, this.subtitle, {this.version});

  @override
  Widget buildLabel(BuildContext context) {

    const kSmallDivider = Divider(
      height: 0.0,
      color: kAccentColor,
      thickness: 2.0,
      indent: 20.0,
    );

    if (title == 'Version' && subtitle == '') {
      return Column(
        children: [
          ListTile(
              tileColor: Theme.of(context).canvasColor,
              minVerticalPadding: 15,
              contentPadding:
              const EdgeInsets.symmetric(vertical: 10.0, horizontal: 16.0),
              dense: true,
              title: Text(title,
                  // tryString!,
                  style: const TextStyle(fontSize: 15.0)),
              trailing: FutureBuilder(
                future: version,
                builder:
                    (BuildContext context, AsyncSnapshot<String> snapshot) =>
                    Text(
                      snapshot.hasData ? snapshot.data! : "No Version Code Set",
                      style: const TextStyle(color: Colors.grey),
                    ),
              ),
              onTap: () {
                onTapMenuRoutes(context, title);
              }),
          kSmallDivider,
        ],
      );
    } else if (subtitle == '') {
      return Column(
        children: [
          ListTile(
              tileColor: Theme.of(context).canvasColor,
              minVerticalPadding: 15,
              contentPadding:
              const EdgeInsets.symmetric(vertical: 10.0, horizontal: 16.0),
              dense: true,
              title: Text(title,
                  // tryString!,
                  style: const TextStyle(fontSize: 15.0)),
              trailing: const Icon(Icons.arrow_forward_ios),
              onTap: () {
                onTapMenuRoutes(context, title);
              }),
          kSmallDivider,
        ],
      );
    } else {
      return Column(
        children: [
          ListTile(
            tileColor: Theme.of(context).canvasColor,
            isThreeLine: true,
            minVerticalPadding: 15,
            contentPadding:
            const EdgeInsets.symmetric(vertical: 10.0, horizontal: 16.0),
            dense: true,
            title: Text(
              title,
              style: const TextStyle(fontSize: 15.0),
            ),
            subtitle: Text(
              subtitle,
              maxLines: 1,
              overflow: TextOverflow.ellipsis,
              style: const TextStyle(fontSize: 12.0),
            ),
            trailing: const Icon(Icons.arrow_forward_ios),
            onTap: () {
              onTapMenuRoutes(context, title);
            },
          ),
          kSmallDivider,
        ],
      );
    }
  }

  @override
  Widget buildSublabel(BuildContext context) => const SizedBox.shrink();
}
