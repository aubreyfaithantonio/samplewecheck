import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:xoo_arkray_new_wecheck_flt/bg.dart';
import 'package:xoo_arkray_new_wecheck_flt/settings.dart';
import 'package:xoo_arkray_new_wecheck_flt/utilities/constants.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:xoo_arkray_new_wecheck_flt/vitals.dart';

import 'input_event_menu_page/input_event_menu_list_page.dart';
import 'main_nav_items/item_bloodglucose.dart';
import 'main_nav_items/item_meal.dart';
import 'main_nav_items/item_other.dart';
import 'main_nav_items/item_settings.dart';
import 'main_nav_items/item_vitals.dart';
import 'meal.dart';
import 'others.dart';

void main(){
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  MyApp({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {

    return const MaterialApp(
        home: MyHomePage(),
        debugShowCheckedModeBanner: false
    );
  }
}

class MyHomePage extends StatefulWidget {
  const MyHomePage({Key? key}) : super(key: key);

  @override
  State<MyHomePage> createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> {
  int selectedIndex = 0;

  final List<Widget> _childrenPage = [
    BloodGlucose(),
    Meal(),
    Vitals(),
    Others(),
    Settings(),
  ];

  void _onTap(int index) {
    selectedIndex = index;
    setState(() {});
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: SafeArea(
        child: IndexedStack(
          index: selectedIndex,
          children: _childrenPage,
        ),
      ),
      bottomNavigationBar: Theme(
        data: ThemeData.from(
          colorScheme: ColorScheme.fromSwatch(primarySwatch: Colors.blueGrey, backgroundColor: Colors.blueGrey.shade200),
        ), child: BottomNavigationBar(
        elevation: 10,
        currentIndex: selectedIndex,
        type: BottomNavigationBarType.fixed,
        onTap: _onTap,
        items: const <BottomNavigationBarItem> [
          BottomNavigationBarItem(
            icon: Icon(Icons.bloodtype),
            activeIcon: Icon(
              Icons.bloodtype,
              color: Colors.red,
            ),
            label: 'Glucose',
          ),
          BottomNavigationBarItem(
              icon: Icon(Icons.set_meal),
              activeIcon: Icon(
                Icons.set_meal,
                color: Colors.blue,
              ),
              label: 'Meal'
          ),
          BottomNavigationBarItem(
              icon: Icon(Icons.accessibility),
              activeIcon: Icon(
                Icons.accessibility,
                color: Colors.orange,
              ),
              label: 'Vitals'
          ),
          BottomNavigationBarItem(
              icon: Icon(Icons.devices_other),
              activeIcon: Icon(
                Icons.devices_other,
                color: Colors.purple
              ),
              label: 'Other'
          ),
          BottomNavigationBarItem(
              icon: Icon(Icons.settings),
              activeIcon: Icon(
                Icons.settings,
                color: Colors.green,
              ),
              label: 'Settings'
          )
        ],
        // selectedItemColor: Colors.blueGrey[900],
        iconSize: 35,
      ),
      ),
    );
  }
}



// void main() {
//   runApp(const SampleWeCheck());
// }
//
// class SampleWeCheck extends StatelessWidget {
//   const SampleWeCheck({Key? key}) : super(key: key);
//
//   @override
//   Widget build(BuildContext context) {
//     return const MaterialApp(
//       title: "Sample WeCheck",
//     );
//   }
// }
//
// class MyHomePage extends StatefulWidget {
//   const MyHomePage({super.key, required this.title});
//
//   final String title;
//
//   @override
//   _MyHomePageState createState() => _MyHomePageState();
// }
//
// class _MyHomePageState extends State<MyHomePage> {
//
//   int _selectedIndex = 0;
//
//   final List<Widget> _Pages = [
//     MainBGPage(),
//     MainVitalsPage(),
//     MainMealPage(),
//     MainOtherPage(),
//     MainSettingsPage()
//   ];
//
//   @override
//   Widget build(BuildContext context) {
//     return Scaffold(
//       appBar: AppBar(
//         title: Text("Sample We Check"),
//         backgroundColor: Colors.blueGrey[900],
//       ),
//     );
//   }
// }






