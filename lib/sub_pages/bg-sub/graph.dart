import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class Graph extends StatefulWidget {
  const Graph({Key? key}) : super(key: key);

  @override
  State<Graph> createState() => _GraphState();
}

class _GraphState extends State<Graph> {
  @override
  Widget build(BuildContext context) {
    return ListView(
      children: [
        ListTile(
          contentPadding: EdgeInsets.all(5.0),
          leading: Icon(Icons.access_time, size: 35.0,),
          title: Text('Time Series Graph'),
          subtitle: Text("Let's see blood glucose levels on time-series graph"),
          trailing: Icon(Icons.arrow_forward_ios),
          onTap: (){
            ScaffoldMessenger.of(context).showSnackBar(
              SnackBar(
                content: const Text('Time-series Graph'),
                action: SnackBarAction(
                  label: 'Okay',
                  onPressed: () {
                    ScaffoldMessenger.of(context).hideCurrentSnackBar();
                  },
                ),
              ),
            );
          },
        ),
        Divider(
          color: Colors.blueGrey[700],
          height: 1,
        ),
        ListTile(
          contentPadding: EdgeInsets.all(5.0),
          leading: Icon(Icons.timeline, size: 35.0,),
          title: Text('Daily Graph'),
          subtitle: Text("Let's see each data on daily graph"),
          trailing: Icon(Icons.arrow_forward_ios),
            onTap: (){
              ScaffoldMessenger.of(context).showSnackBar(
                SnackBar(
                  content: const Text('Daily Graph'),
                  action: SnackBarAction(
                    label: 'Okay',
                    onPressed: () {
                      ScaffoldMessenger.of(context).hideCurrentSnackBar();
                    },
                  ),
                ),
              );
            }
        ),
        Divider(
          color: Colors.blueGrey[700],
          height: 1,
        ),
        ListTile(
          contentPadding: EdgeInsets.all(5.0),
          leading: Icon(Icons.pie_chart, size: 35.0,),
          title: Text('Circadian Variation Graph'),
          subtitle: Text("Let's see blood glucose levels on circadian variation graph"),
          trailing: Icon(Icons.arrow_forward_ios),
            onTap: (){
              ScaffoldMessenger.of(context).showSnackBar(
                SnackBar(
                  content: const Text('Circadian Variation Graph'),
                  action: SnackBarAction(
                    label: 'Okay',
                    onPressed: () {
                      ScaffoldMessenger.of(context).hideCurrentSnackBar();
                    },
                  ),
                ),
              );
            }
        ),
        Divider(
          color: Colors.blueGrey[700],
          height: 1,
        )
      ],
    );
  }
}
